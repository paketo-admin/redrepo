package com.paketo.BitbucketWebhook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication

public class BitbucketWebhookApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(BitbucketWebhookApplication.class, args);
	}
}
